package `in`.jayesh.kotlinexample.Sqlite

import `in`.jayesh.kotlinexample.R
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.*
import java.text.SimpleDateFormat
import java.util.*

class InsertActivity : AppCompatActivity() {
    lateinit var save : Button
    lateinit var edtFname : EditText
    lateinit var edtLname : EditText

    lateinit var  databaseHelper: DatabaseHelper

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_insert)

        save=findViewById(R.id.btn_save)
        edtFname=findViewById(R.id.edt_fname)
        edtLname=findViewById(R.id.edt_lname)
        databaseHelper=DatabaseHelper(this)

        save.setOnClickListener {
            insertFunction()
        }

    }


    private fun insertFunction() {

        val fname=edtFname.text.toString()
        val lname=edtLname.text.toString()

        val user=User()
        user.fname=fname
        user.lname=lname

        val result : Boolean = databaseHelper.onStoreData(user)

        when{
            result->{
                Toast.makeText(this,"Data inserted Successfully..",Toast.LENGTH_LONG).show()
                finish()
            }
            else->Toast.makeText(this,"Failed to insert data",Toast.LENGTH_LONG).show()
        }
    }
}
