package `in`.jayesh.kotlinexample.Sqlite

import `in`.jayesh.kotlinexample.R
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.content.Intent
import android.widget.Toast


class DataAdapter(val viewAllDataActivity: ViewAllDataActivity,val arrayList: ArrayList<HashMap<String, String>>) : RecyclerView.Adapter<DataAdapter.ViewHolder>() {
lateinit var helper : DatabaseHelper
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val vi=LayoutInflater.from(viewAllDataActivity).inflate(R.layout.cust_layout,parent,false)
        return ViewHolder(vi)
    }

    override fun getItemCount(): Int {
        return arrayList.size
        Log.d("size",arrayList.size.toString())
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.name.text=arrayList.get(position).get(FNAME)
        holder.name2.text=arrayList.get(position).get(LNAME)
        helper= DatabaseHelper(viewAllDataActivity)
        Log.d("hiiii", arrayList.toString())

        holder.update.setOnClickListener {
            val intent = Intent(viewAllDataActivity, UpdateActivity::class.java)
            intent.putExtra(ID, arrayList.get(position).get(ID))
            intent.putExtra(FNAME,arrayList.get(position).get(FNAME))
            intent.putExtra(LNAME, arrayList.get(position).get(LNAME))
            viewAllDataActivity.startActivity(intent)
        }

        holder.delete.setOnClickListener {
           val result : Boolean = helper.deleteUser(Integer.parseInt(arrayList.get(position).get(ID)))

            when{
                result->{
                    Toast.makeText(viewAllDataActivity,"Data deleted Successfully..", Toast.LENGTH_LONG).show()
                    val intent = Intent(viewAllDataActivity, ViewAllDataActivity::class.java)
                    viewAllDataActivity.startActivity(intent)
                }
                else-> Toast.makeText(viewAllDataActivity,"Failed to delete data", Toast.LENGTH_LONG).show()
            }

        }
    }

    class ViewHolder(item : View) : RecyclerView.ViewHolder(item) {
        val name=item.findViewById<TextView>(R.id.txt_name)
        val name2=item.findViewById<TextView>(R.id.txt2_name)
        val update=item.findViewById<Button>(R.id.btn_update)
        val delete=item.findViewById<Button>(R.id.btn_delete)
    }

    companion object {
        private val ID = "Id"
        private val FNAME = "FName"
        private val LNAME= "LName"
    }
}